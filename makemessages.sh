#!/usr/bin/env bash
#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#

project="proplan"

log_critical() { echo -e "\e[41m${1}\e[0m"; };
log_error() { echo -e "\e[31m${1}\e[0m"; };
log_warn() { echo -e "\e[33m${1}\e[0m"; };
log_info() { echo -e "\e[32m${1}\e[0m"; };

if [ -f "${project}/__init__.py" ]
then
    VERSION=$(python -c "import ${project}; print(${project}.__version__);");
else
    log_critical "This script must be run from it directory!";
    exit 1;
fi;

if [ ! -x "$(which django-admin)" ]
then
    log_critical "Please setup the 'django' for system:";
    log_critical "sudo apt-get install python3-django";
    exit 1;
fi;

BASE_DIR="$(pwd)";

build_app() {
    if [ -d "${1}/locale/" ]
    then
        cd ${1};
        # TODO: Add other languages here.
        for lang in "en" "ru"
        do
            django-admin makemessages -l "${lang}" > /dev/null;
            # Check for changes. 1 line indicates that only time has changed.
            dst="${1}/locale/${lang}/LC_MESSAGES/django.po";
            ins=$(git diff --shortstat ${dst} | awk '{print $4}');
            if [[ "${ins}" == "" || "${ins}" == "1" ]]
            then
                log_info "      OK: ${dst}";
                git checkout -- "${dst}" > /dev/null;
            else
                log_warn "NEED FIX: ${dst} ${ins} lines."
            fi;
        done;
    fi;
}

echo "BEGIN";
echo " VERSION: ${VERSION}";

build_app "${BASE_DIR}/${project}";

echo "END";
exit 0;
