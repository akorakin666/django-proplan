/*
 * selectizers.js
 *
 * Autosetup selectize fields.
 *
 */


$(document).ready(function($) {
    $('.selectize-is-not-set').each(function(index, select) {
        var $select = $(select),
            data = $select.data(),
            pk = data.pk || 'pk',
            label = data.label || 'text',
            opts = {};
        if (data.url) {
            var filters = {},
                fields = (data.fields || 'text').split(',');
            $.each((data.filters || '').split('&'), function(i, p) {
                var kv = p.split('=');
                filters[kv[0]] = kv[1];
            })
            opts.load = function(query, callback) {
                var params = $.extend({}, filters);
                $.each(params, function(k, v) {
                    v = decodeURIComponent(v);
                    if (v.startsWith('$(')) {
                        params[k] = eval(v) || undefined;
                    }
                })
                params.q = query;
                $.getJSON(data.url, params, function(res) {
                    if (res.page && res.page.objects) {
                        callback(res.page.objects)
                    }
                    else {
                        callback(res)
                    }
                });
            }
            opts.valueField = pk;
            opts.searchField = fields;
            opts.preload = 'focus';
            opts.render = {
                item: function(item, escape) {
                    var text = item[label] ? item[label] : item.text;
                    return '<div>' + escape(text) + '</div>';
                },
                option: function(item, escape) {
                    var value = item[pk] ? item[pk]: item.value,
                        text = item[label] ? item[label] : item.text,
                        add = [],
                        html = '<div class="mx-2">'
                            + '<span class="pull-right"> #'
                            + escape(value) + '</span>'
                            + escape(text);
                    $.each(fields, function(n, f) {
                        if (f != label) {
                            var v = item[f];
                            if (v) add.push(escape(v));
                        }
                    })
                    if (add.length) {
                        html += '<p class="mb-0 pl-2 text-secondary">' +
                                add.join('<br>') + '</p>'
                    }
                    return html + '</div>'
                }
            }
        };
        if (data.create) {
            opts.createFilter = function(input) {
                return Boolean(input);
            };
            opts.create = function(input) {
                if (input) {
                    return {
                        value: 'new:' + input,
                        text: input,
                    };
                }
                return false;
            }
        }
        else {
            opts.create = false;
        }
        $select.removeClass('selectize-is-not-set').selectize(opts);
    })
})
