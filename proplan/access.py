#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
"""
Functions for access control.
"""
from django.contrib.auth import get_user_model
from proplan.conf import ABS_COOKIE_NAME, ABS_KEYS


def authenticated(user):
    """For authenticated users."""
    return user.is_active and user.is_authenticated


def staff(user):
    """For employers and superusers."""
    return authenticated(user) and (user.is_staff or user.is_superuser)


def superuser(user):
    """For superusers only."""
    return authenticated(user) and user.is_superuser


User = get_user_model()
view_perm = '%s.view_%s' % (User._meta.app_label, User._meta.model_name)


def view_users(user):
    """For users with view permission for User model."""
    return authenticated(user) and user.has_perm(view_perm)


def view_task(user):
    """For users with view permission for Task model."""
    return authenticated(user) and user.has_perm('proplan.view_task')


def check_abs_key(request):
    """Checks the request for Automatic Bug System."""
    key = request.COOKIES.get(ABS_COOKIE_NAME, '')
    if key in ABS_KEYS:
        request.META['ABS_KEY'] = key
        return True
    if 'ABS_KEY' in request.META:
        request.META.pop('ABS_KEY')
    return False
