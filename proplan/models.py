#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
import json
import re
from os import (
    path as os_path, remove as os_remove, removedirs as os_removedirs,
)
from unidecode import unidecode

from django.conf import settings
from django.contrib.auth.models import Permission
from django.core.cache import caches
from django.db import models
from django.template.loader import render_to_string
from django.utils.crypto import get_random_string
from django.utils.functional import cached_property
from django.utils.timezone import now, timedelta
from django.utils.translation import gettext_lazy as _

from proplan.conf import (
    ATTACH_UPLOAD_PATH, ATTACH_THUMB_EXTENSIONS, PRIORITIES,
    DEADLINE_DAYS_PRIORITIES
)

try:
    cache = caches['proplan']
except Exception:
    cache = caches['default']


class Parameter(models.Model):
    """
    Parameters of Proplan, changed without restart server.
    """
    CACHE_PREFIX = 'proplan_param_'
    VALUE_STRING = 1
    VALUE_INTEGER = 2
    VALUE_FLOAT = 3

    VALUE_CHOICES = (
        (VALUE_STRING, 'string'),
        (VALUE_INTEGER, 'integer'),
        (VALUE_FLOAT, 'float'),
    )
    # Unique name of parameter by user.
    name = models.CharField(_('название'), max_length=50)
    # If no user, then it as system parameter.
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=True, null=True,
        verbose_name=_('пользователь'),
        related_name='proplan_parameters')
    data = models.TextField(_('данные'), blank=True)
    datatype = models.SmallIntegerField(_('тип данных'), choices=VALUE_CHOICES)
    timeout = models.SmallIntegerField(
        _('время кэширования'), default=600,
        help_text=_('измеряется в секундах'),
    )

    class Meta:
        verbose_name = _('параметр')
        verbose_name_plural = _('параметры')
        ordering = ('name',)
        unique_together = ('name', 'user')

    def __str__(self):
        return self.name

    def save(self, **kwargs):
        self.prepare_data()
        super(Parameter, self).save(**kwargs)
        prefix = self.CACHE_PREFIX
        key = self.make_key
        cache.delete_many([
            '%s_%s' % (prefix, key(self.name, self.user_id)),
            '%s_%s' % (prefix, key(None, self.user_id)),
        ])

    @staticmethod
    def make_key(name=None, user_id=None):
        return '%s_%d' % (name if name else 'all', user_id if user_id else 0)

    @classmethod
    def prepare_value(cls, datatype, value):
        datatype = int(datatype)
        if datatype == cls.VALUE_STRING:
            return str(value)
        elif datatype == cls.VALUE_INTEGER:
            return int(value)
        elif datatype == cls.VALUE_FLOAT:
            return float(value)
        return value

    @property
    def value(self):
        return Parameter.prepare_value(self.datatype, self.data)

    def prepare_data(self):
        value = self.data
        assert value and isinstance(value, str), (
            'Value must be not blank string.'
        )
        value = value.replace('\n', ' ')
        value = value.replace('\t', ' ')
        value = re.sub(r'\s+', ' ', value)
        value = value.strip()
        if self.datatype != self.VALUE_STRING:
            assert value, 'Value must be not blank string.'
        if self.datatype == self.VALUE_INTEGER:
            value = int(value)
        elif self.datatype == self.VALUE_FLOAT:
            value = float(value)
        self.data = str(value)

    @classmethod
    def get_value(cls, name, user_id=None, default=None, force=False):
        cache_key = '%s_%s' % (cls.CACHE_PREFIX, cls.make_key(name, user_id))
        if force:
            value = None
        else:
            value = cache.get(cache_key)
        if value is None:
            param = Parameter.objects.filter(name=name, user=user_id).first()
            if param:
                value = param.value
                timeout = param.timeout
                to_cache = '%d:%s' % (param.datatype, param.data)
            else:
                timeout = 600
                to_cache = '0:0'
            # Caching for the period specified in the existing parameter,
            # and in his absence for an hour.
            cache.set(cache_key, to_cache, timeout)
        else:
            # There 0 - is None
            datatype = value[:1]
            if datatype == '0':
                return default
            value = cls.prepare_value(datatype, value[2:])
        if default is not None and value is None:
            return default
        return value

    @classmethod
    def get_all_params(cls, user_id=None):
        cache_key = '%s_%s' % (cls.CACHE_PREFIX, cls.make_key(None, user_id))
        value = cache.get(cache_key)
        if value is None:
            qs = Parameter.objects.filter(user_id=user_id)
            value = qs.values_list('name', flat=True)
            to_cache = ';'.join(value)
            # caching for 10 minutes: 60 * 10
            cache.set(cache_key, to_cache, 600)
        elif value:
            value = value.split(';')
        else:
            value = []
        get_value = cls.get_value
        return {k: get_value(k, user_id) for k in value}


def upload_attach(instance, filename):
    filename = unidecode(filename).lower()
    filename = filename.replace(' ', '_').replace("'", '').replace('"', '')
    dic = {
        'filename': filename,
        'date': now().date().isoformat(),
        'code': get_random_string(),
        'user_id': instance.user_id or 0,
    }
    return ATTACH_UPLOAD_PATH % dic


class AttachmentManager(models.Manager):
    def get_free(self):
        qs = self.get_queryset()
        qs = qs.filter(task__isnull=True, comment__isnull=True)
        return qs


class Attachment(models.Model):
    """Model of uploaded files."""

    created = models.DateTimeField(
        _('добавлено'), auto_now_add=True, db_index=True)
    updated = models.DateTimeField(
        _('обновлено'), auto_now=True, db_index=True)
    # Who uploaded this file. The Automatic Bugs System sets as None.
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        blank=True, null=True,
        verbose_name=_('пользователь'),
        related_name='proplan_attaches')
    file = models.FileField(_('файл'), upload_to=upload_attach)

    objects = AttachmentManager()

    class Meta:
        verbose_name = _('вложение')
        verbose_name_plural = _('вложения')
        ordering = ('-updated', '-created')

    def __str__(self):
        return os_path.basename(self.file.name)

    def delete(self, **kwargs):
        """Clears the directory of files before deleting the object."""
        if self.use_thumbnail:
            try:
                os_remove(self.thumb_path)
            except OSError:
                pass
        file = self.file
        path = file.path
        file.delete(False)
        try:
            os_removedirs(os_path.dirname(path))
        except OSError:
            pass
        super(Attachment, self).delete(**kwargs)

    @staticmethod
    def get_thumb_filename(filename):
        """Returns the name of the thumbnail created from the file name."""
        return '%s.thumb%s' % os_path.splitext(filename)

    @property
    def is_abs(self):
        """True if it uploaded from automatic bug system."""
        return not self.user_id

    @property
    def extension(self):
        """Returns extension for file in lower case."""
        root, ext = os_path.splitext(self.file.name)
        return ext.lower()

    @property
    def is_image(self):
        """Returns True if the file is an image."""
        L = (
            '.jpg', '.jpeg', '.png', '.bmp', '.gif', '.svg',
        )
        return self.extension in L

    @property
    def use_thumbnail(self):
        """Returns True if the file is an image with thumbnail."""
        return self.extension in ATTACH_THUMB_EXTENSIONS

    @property
    def name(self):
        """Returns the cleaned name of file without path."""
        return os_path.basename(self.file.name)

    @property
    def url(self):
        return self.file.url

    @property
    def thumb_url(self):
        if self.use_thumbnail:
            return self.get_thumb_filename(self.file.url)
        return ''

    @property
    def path(self):
        return self.file.path

    @property
    def thumb_path(self):
        if self.use_thumbnail:
            return self.get_thumb_filename(self.file.path)
        return ''

    def get_tasks(self):
        return self.task_set.all()

    def get_comments(self):
        return self.comment_set.all()


class TrackerManager(models.Manager):
    def get_abs(self):
        qs = self.get_queryset()
        return qs.filter(is_abs=True).first()


class Tracker(models.Model):
    """
    Model of one tracker.

    By default exists "development", "support" and "bugs".
    The "development" is a wish status. The "support" is a need status.
    The "bugs" is a defect status.
    """
    # The higher the status, the more important.
    STATUS_WISH = 1
    STATUS_NEED = 2
    STATUS_DEFECT = 3
    STATUS_CHOICES = (
        (STATUS_WISH, _('пожелание')),
        (STATUS_NEED, _('необходимость')),
        (STATUS_DEFECT, _('дефект')),
    )
    name = models.CharField(_('название'), max_length=16, db_index=True)
    description = models.TextField(_('описание'), blank=True)
    status = models.PositiveSmallIntegerField(
        _('статус'), default=1, choices=STATUS_CHOICES, blank=True)
    is_abs = models.BooleanField(
        _('для автоматической системы ошибок'), default=False,
        help_text=_('Только одна запись может быть установлена для ABS.'))

    objects = TrackerManager()

    class Meta:
        ordering = ('id',)
        verbose_name = _('трекер')
        verbose_name_plural = _('трекеры')

    def __str__(self):
        return self.name

    def save(self, **kwargs):
        super(Tracker, self).save(**kwargs)
        if self.is_abs:
            qs = Tracker.objects.filter(is_abs=True).exclude(id=self.id)
            qs.update(is_abs=False)

    @property
    def is_wish(self):
        return self.status == self.STATUS_WISH

    @property
    def is_need(self):
        return self.status == self.STATUS_NEED

    @property
    def is_defect(self):
        return self.status == self.STATUS_DEFECT


class StageManager(models.Manager):
    def get_abs(self):
        qs = self.get_queryset()
        return qs.filter(is_abs=True).first()


class Stage(models.Model):
    """
    Model of one stage.
    """
    STATUS_NEW = 1
    STATUS_ERROR = 2
    STATUS_PROCESS = 3
    STATUS_PAUSE = 4
    STATUS_FINISH = 5
    STATUS_CHOICES = (
        (STATUS_NEW, _('для новых задач')),
        (STATUS_ERROR, _('для задач с ошибками')),
        (STATUS_PROCESS, _('для исполняемых задач')),
        (STATUS_PAUSE, _('для приостановки задач')),
        (STATUS_FINISH, _('для завершения задач')),
    )
    STATUSES_FOR_PARENTS = (STATUS_NEW, STATUS_PROCESS, STATUS_PAUSE)
    name = models.CharField(_('название'), max_length=16, db_index=True)
    description = models.TextField(_('описание'), blank=True)
    status = models.PositiveSmallIntegerField(
        _('статус'), default=1, choices=STATUS_CHOICES, blank=True)
    is_abs = models.BooleanField(
        _('для автоматической системы ошибок'), default=False,
        help_text=_('Только одна запись может быть установлена для ABS.'),
    )

    objects = StageManager()

    class Meta:
        ordering = ('id',)
        verbose_name = _('стадия')
        verbose_name_plural = _('стадия')

    def __str__(self):
        return self.name

    def save(self, **kwargs):
        if self.is_abs and not self.is_new:
            self.is_abs = False
        super(Stage, self).save(**kwargs)
        if self.is_abs:
            qs = Stage.objects.filter(is_abs=True).exclude(id=self.id)
            qs.update(is_abs=False)

    @property
    def is_new(self):
        return self.status == self.STATUS_NEW

    @property
    def is_error(self):
        return self.status == self.STATUS_ERROR

    @property
    def is_process(self):
        return self.status == self.STATUS_PROCESS

    @property
    def is_pause(self):
        return self.status == self.STATUS_PAUSE

    @property
    def is_finish(self):
        return self.status == self.STATUS_FINISH


class Subproject(models.Model):
    """
    Model of one subproject.
    """
    name = models.CharField(_('название'), max_length=50, db_index=True)
    description = models.TextField(_('описание'), blank=True)

    class Meta:
        ordering = ('name',)
        verbose_name = _('подпроект')
        verbose_name_plural = _('подпроекты')

    def __str__(self):
        return self.name


class Role(models.Model):
    """
    Model of one user role.
    """
    name = models.CharField(_('название'), max_length=50, db_index=True)
    description = models.TextField(_('описание'), blank=True)
    users = models.ManyToManyField(
        settings.AUTH_USER_MODEL, blank=True, verbose_name=_('пользователи'))
    permissions = models.ManyToManyField(
        Permission,
        verbose_name=_('разрешения'),
        blank=True,
        related_name='proplan_roles',
        limit_choices_to={'content_type__app_label': 'proplan'},
    )

    class Meta:
        ordering = ('name',)
        verbose_name = _('роль пользователя')
        verbose_name_plural = _('роли пользователей')

    def __str__(self):
        return self.name


class Version(models.Model):
    """
    Model of versions.
    """
    major = models.PositiveSmallIntegerField(
        _('мажорная'), default=0, db_index=True)
    minor = models.PositiveSmallIntegerField(
        _('минорная'), default=0, db_index=True)
    bugfix = models.CharField(
        _('багфикс'), max_length=16, blank=True, db_index=True)
    # The planned date for release.
    planned_date = models.DateTimeField(
        _('планируемая дата'), null=True, blank=True, db_index=True)
    # The real date for release.
    release_date = models.DateTimeField(
        _('дата выхода'), null=True, blank=True, db_index=True)
    # Master is a responsible for this version.
    master = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        blank=True, null=True,
        verbose_name=_('ответственный за выпуск'),
        related_name='proplan_master_versions',
    )
    # About the version.
    description = models.TextField(_('описание'), blank=True)
    # The list of closed tasks for format to readable text.
    changelog = models.TextField(_('список изменений'), blank=True)

    class Meta:
        ordering = ('-id',)
        verbose_name = _('версия')
        verbose_name_plural = _('версии')
        unique_together = ('major', 'minor', 'bugfix')

    def __str__(self):
        return self.version

    @property
    def version(self):
        if self.bugfix:
            return '%d.%d.%s' % (self.major, self.minor, self.bugfix)
        return '%d.%d' % (self.major, self.minor)

    def make_changelog(self):
        """Created new text of changelog."""
        lines = []
        tasks = self.task_set.filter(stage__status=Stage.STATUS_FINISH)
        tasks = tasks.order_by('-tracker__status', '-priority', 'updated')
        for t in tasks:
            executors = t.get_executors_names()
            lines.append(
                '[%s] %s (%s)' % (t.tracker, t.title, executors)
            )
        text = '\n'.join(lines)
        self.changelog = text

    @property
    def count_total_tasks(self):
        return self.task_set.count()

    @property
    def count_open_tasks(self):
        qs = self.task_set.exclude(stage__status=Stage.STATUS_FINISH)
        qs = qs.exclude(stage__status=Stage.STATUS_ERROR)
        return qs.count()

    @property
    def count_reject_tasks(self):
        qs = self.task_set.filter(stage__status=Stage.STATUS_ERROR)
        return qs.count()

    @property
    def count_finish_tasks(self):
        qs = self.task_set.filter(stage__status=Stage.STATUS_FINISH)
        return qs.count()

    @property
    def readiness(self):
        """Readiness the release in percent."""
        total = self.count_total_tasks
        if total:
            return 100 - (100 / total * self.count_open_tasks)
        return 0.0

    def get_master_full_name(self):
        master = self.master
        if master:
            return master.get_full_name() or str(master)
        return _('отсутствует')


class Task(models.Model):
    """
    Model of one task.
    """
    created = models.DateTimeField(_('создана'), auto_now_add=True)
    updated = models.DateTimeField(_('обновлена'), auto_now=True)
    # Author of an idea or a bug found. The Automatic Bugs System sets as None.
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        blank=True, null=True,
        verbose_name=_('автор'),
        related_name='proplan_author_tasks',
    )
    # Leader is responsible for this task.
    leader = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        blank=True, null=True,
        verbose_name=_('ответственный за задачу'),
        related_name='proplan_leader_tasks',
    )
    # The parent task.
    parent = models.ForeignKey(
        'Task',
        on_delete=models.SET_NULL,
        blank=True, null=True,
        related_name='children',
        verbose_name=_('pодительская задача'),
    )
    # The subproject of main project.
    subproject = models.ForeignKey(
        Subproject,
        on_delete=models.SET_NULL,
        blank=True, null=True,
        verbose_name=_('подпроект'),
    )
    # The version of project.
    version = models.ForeignKey(
        Version,
        on_delete=models.SET_NULL,
        blank=True, null=True,
        verbose_name=_('версия'),
    )
    # Tracker is an idea, a bug or something else.
    tracker = models.ForeignKey(Tracker, verbose_name=_('трекер'),
                                on_delete=models.CASCADE)
    # The stage of current.
    stage = models.ForeignKey(Stage, verbose_name=_('стадия'),
                              on_delete=models.CASCADE)
    # The higher the priority, the more important it is for the project.
    priority = models.SmallIntegerField(
        _('приоритет'), choices=PRIORITIES, default=1, db_index=True)
    # True if it received from automatic bug system.
    is_abs = models.BooleanField(
        _('из автоматической системы ошибок'), default=False, db_index=True)
    # Title of the task.
    title = models.CharField(_('наименование'), max_length=255, db_index=True)
    # Detailed description of the task.
    description = models.TextField(_('описание'), blank=True)
    # The planned date for start work.
    planned_start = models.DateTimeField(
        _('начало работ'), null=True, blank=True)
    # The planned time work in seconds.
    planned_time = models.IntegerField(
        _('трудозатраты'), null=True, blank=True)
    # The files attached by the author of the task.
    attachments = models.ManyToManyField(
        Attachment, blank=True, verbose_name=_('attachments'))

    class Meta:
        verbose_name = _('задача')
        verbose_name_plural = _('задачи')
        ordering = ('-updated', '-created')

    def __str__(self):
        return self.title or _('новая')

    @property
    def time_total(self):
        """Returns the seconds spended for total work."""
        qs = Executor.objects.filter(task=self)
        seconds = sum([e.time_total for e in qs])
        return seconds

    @property
    def time_work(self):
        """Returns the seconds spended for work on sprints."""
        qs = Executor.objects.filter(task=self)
        seconds = sum([e.time_work for e in qs])
        return seconds

    @property
    def planned_days(self):
        if not self.planned_time:
            return None
        return self.planned_time // 86400

    @property
    def planned_hours(self):
        if not self.planned_time:
            return None
        hours = self.planned_time - self.planned_time // 86400
        return hours // 3600

    @property
    def planned_minutes(self):
        if not self.planned_time:
            return None
        minutes = self.planned_time - self.planned_time // 3600
        return minutes // 60

    @cached_property
    def sorted_attachments(self):
        qs = self.attachments.order_by('file')
        return list(qs)

    @cached_property
    def start(self):
        """
        Returns the start of the work of the first executor.
        """
        qs = Executor.objects.filter(task=self, start__isnull=False)
        first = qs.order_by('start').first()
        return first.start if first else self.planned_start

    @cached_property
    def stop(self):
        """
        Returns the end of the work of the last executor.
        """
        qs = Executor.objects.filter(task=self, stop__isnull=False)
        last = qs.order_by('-stop').first()
        return last.stop if last else None

    @cached_property
    def deadline(self):
        seconds = 0
        days = 0
        start = self.created
        if self.planned_start and self.planned_time:
            seconds = self.planned_time
            start = self.planned_start
        elif self.planned_start:
            days = DEADLINE_DAYS_PRIORITIES.get(self.priority, 30)
            start = self.planned_start
        elif self.planned_time:
            days = DEADLINE_DAYS_PRIORITIES.get(self.priority, 30)
            seconds = self.planned_time
        elif self.version and self.version.planned_date:
            return self.version.planned_date
        else:
            days = DEADLINE_DAYS_PRIORITIES.get(self.priority, 30)
        deadline = start + timedelta(days=days) + timedelta(seconds=seconds)
        return deadline

    @cached_property
    def deadline_profit(self):
        if self.stage.status != Stage.STATUS_FINISH:
            return
        deadline = self.deadline
        stop = self.stop
        return (deadline - stop).total_seconds() if stop else None

    def get_executors_names(self):
        return ', '.join(sorted([
            e.user.get_full_name() or e.user.username
            for e in self.executors.all()
        ]))

    def get_author_full_name(self):
        author = self.author
        return author.get_full_name() or str(author)

    def get_leader_full_name(self):
        leader = self.leader
        if leader:
            return leader.get_full_name() or str(leader)
        return _('отсутствует')

    def get_master_full_name(self):
        version = self.version
        if version:
            return version.get_master_full_name()
        return _('отсутствует')

    @property
    def comments(self):
        qs = self.comment_set.all()
        qs = qs.order_by('id')
        return qs


class CommentsManager(models.Manager):
    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(message__gt='')


class Comment(models.Model):
    """
    Model of one comment for task.
    """
    created = models.DateTimeField(_('добавлен'), auto_now_add=True)
    updated = models.DateTimeField(_('обновлён'), auto_now=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        verbose_name=_('пользователь'),
        related_name='proplan_comments',
    )
    task = models.ForeignKey(
        Task, on_delete=models.CASCADE, verbose_name=_('задача'))
    message = models.TextField(_('сообщение'), db_index=True)
    # JSON data about upgrading the task.
    changes = models.TextField(_('изменения задачи'), blank=True, editable=False)
    attachments = models.ManyToManyField(
        Attachment, blank=True, verbose_name=_('вложения'))

    objects = models.Manager()
    comments = CommentsManager()

    class Meta:
        verbose_name = _('комментарий')
        verbose_name_plural = _('комментарии')
        ordering = ('-updated', '-created')

    def __str__(self):
        return _('#%(id)d от пользователя "%(user)s"') % {
            'id': self.id, 'user': self.user
        }

    def changes_as_html(self):
        try:
            changes = json.loads(self.changes or '{}')
        except Exception:
            changes = {}
        params = {
            'author': _('автор'),
            'leader': _('ответственный'),
            'parent': _('родительская задача'),
            'subproject': _('подпроект'),
            'version': _('версия'),
            'tracker': _('трекер'),
            'stage': _('стадия'),
            'priority': _('приоритет'),
            'is_abs': _('из ABS'),
            'title': _('наименование'),
            'description': _('описание'),
            'planned_start': _('начало работ'),
            'planned_time': _('трудозатраты'),
            'attachments': _('вложения'),
            # Form fields:
            'planned_days': _('дней по плану'),
            'planned_hours': _('часов по плану'),
            'planned_minutes': _('минут по плану'),
        }
        L = []
        for k, v in changes.items():
            name = params.get(k, k)
            L.append([name, *v[:2]])
        ctx = {
            'task': self.task,
            'changes': L,
        }
        return render_to_string('proplan/_task_changes.html', ctx)


class Executor(models.Model):
    """
    Model of one executor for task.
    """
    created = models.DateTimeField(_('добавлен'), auto_now_add=True)
    updated = models.DateTimeField(_('обновлён'), auto_now=True)
    task = models.ForeignKey(
        Task,
        on_delete=models.PROTECT,
        related_name='executors',
        verbose_name=_('задача'),
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        verbose_name=_('пользователь'),
        related_name='proplan_executors',
    )
    role = models.ForeignKey(
        Role,
        on_delete=models.PROTECT,
        blank=True, null=True,
        verbose_name=_('роль пользователя'),
        related_name='executors',
    )
    # Start working on the task.
    start = models.DateTimeField(
        _('начало работы'), null=True, blank=True, editable=False,
        db_index=True)
    # End working on the task.
    stop = models.DateTimeField(
        _('окончание работы'), null=True, blank=True, editable=False,
        db_index=True)
    # The start time for the current sprint. This should be absent when there
    # is no work.
    sprint = models.DateTimeField(
        _('начало спринта'), null=True, blank=True, editable=False)
    # The time of sprints in the seconds.
    time_sprints = models.IntegerField(
        _('затраченное время'), default=0, editable=False, db_index=True)

    class Meta:
        verbose_name = _('исполнитель')
        verbose_name_plural = _('исполнители')
        ordering = ('-updated', '-created')
        permissions = (
            ('start_other_executor', _('Может запустить другого исполнителя')),
            ('stop_other_executor', _('Может остановить другого исполнителя')),
        )

    def __str__(self):
        return '%s: %s' % (self.get_role_name(), self.get_full_name())

    def get_full_name(self):
        user = self.user
        return user.get_full_name() or str(user)

    @property
    def time_total(self):
        """Returns the seconds spended for total work."""
        start = self.start
        stop = self.stop
        if start is None:
            return 0
        if stop is None:
            stop = now()
        return (stop - start).total_seconds()

    @property
    def time_work(self):
        """Returns the seconds spended for work on sprints."""
        time = self.time_sprints
        sprint = self.sprint
        if sprint:
            time += (now() - sprint).total_seconds()
        return time

    @property
    def is_current(self):
        return bool(self.sprint)

    def start_sprint(self):
        """Sets the start of the new sprint."""
        assert not self.sprint
        stop_other = Parameter.get_value('stop_other_executors', default='all')
        if stop_other:
            # Stop the other user executions.
            qs = Executor.objects.filter(user=self.user, sprint__isnull=False)
            if stop_other == 'task':
                qs = qs.filter(task=self.task)
            for e in qs:
                e.stop_sprint()
        if not self.start:
            self.start = now()
        self.sprint = now()
        self.save()
        return self.sprint

    def stop_sprint(self):
        """Stops the current sprint."""
        sprint = self.sprint
        assert sprint
        _now = now()
        time = (_now - sprint).total_seconds()
        self.time_sprints += time
        self.sprint = None
        self.stop = _now
        self.save()
        return time

    def get_role_name(self):
        """The role of the executor by default."""
        if self.role:
            return str(self.role)
        return _('Исполнитель')
