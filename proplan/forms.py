# -*- coding: utf-8 -*-
import json

from django import forms
from django.db.models import Q
from django.contrib.auth import get_user_model
from django.forms.utils import from_current_timezone
from django.utils.encoding import force_text
from django.utils.dateparse import parse_datetime
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from proplan import models, conf
from proplan.logic import create_attachment, similarity_checking
from proplan.widgets import Selectize, SelectizeMultiple


class IsoDateTimeField(forms.DateTimeField):
    """DateTimeField with support ISO format."""

    def to_python(self, value):
        # First parse from ISO format.
        if value and isinstance(value, str):
            result = parse_datetime(value)
            if result:
                return from_current_timezone(result)
        # If not ISO format then standard behavior.
        return super().to_python(value)


class ProfileForm(forms.ModelForm):

    class Meta:
        model = get_user_model()
        fields = conf.PROFILE_FIELDS


class AttachmentForm(forms.Form):
    file = forms.FileField(required=True)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(AttachmentForm, self).__init__(*args, **kwargs)

    def clean_file(self):
        file = self.cleaned_data['file']
        if not hasattr(file, 'content_type'):
            raise forms.ValidationError(_('Файл не содержит типа контента.'))
        name = file.name
        if '.' not in name:
            raise forms.ValidationError(_('Имя файла не содержит расширения.'))
        if '/' in name:
            raise forms.ValidationError(_('Имя файла содержит слеш.'))
        return file

    def save(self):
        file = self.cleaned_data['file']
        return create_attachment(self.user, file)


class TrackerForm(forms.ModelForm):

    class Meta:
        model = models.Tracker
        exclude = []


class StageForm(forms.ModelForm):

    class Meta:
        model = models.Stage
        exclude = []


class SubprojectForm(forms.ModelForm):

    class Meta:
        model = models.Subproject
        exclude = []


class RoleForm(forms.ModelForm):

    class Meta:
        model = models.Role
        exclude = []


class VersionForm(forms.ModelForm):

    planned_date = IsoDateTimeField(required=False, label=_('Планируемая дата'))
    release_date = IsoDateTimeField(required=False, label=_('Дата релиза'))

    class Meta:
        model = models.Version
        exclude = ['changelog']


class CheckTaskMixing:

    def similarity_checking(self, cleaned_data):
        task = self.instance
        other = similarity_checking(
            cleaned_data.get('title', task.title),
            cleaned_data.get('description', task.description),
            task.is_abs,
            exclude_id=task.id,
        )
        if other:
            raise forms.ValidationError(
                _('Подобные задачи существуют: %(other)s.'),
                params={'other': other}
            )


class TaskCreateForm(CheckTaskMixing, forms.ModelForm):

    planned_start = IsoDateTimeField(required=False, label=_('Начало работ'))
    planned_time = forms.IntegerField(
        required=False, widget=forms.HiddenInput())
    planned_days = forms.IntegerField(
        required=False, label=_('Дней по плану'), min_value=1)
    planned_hours = forms.IntegerField(
        required=False, label=_('Часов по плану'), min_value=1, max_value=23)
    planned_minutes = forms.IntegerField(
        required=False, label=_('Минут по плану'), min_value=1, max_value=59)

    class Meta:
        model = models.Task
        exclude = [
            'created', 'updated', 'author', 'is_abs',
        ]
        widgets = {
            'parent': Selectize(),
            'leader': Selectize(),
            'subproject': Selectize(),
            'version': Selectize(),
            'attachments': SelectizeMultiple(),
        }

    def __init__(self, user, *args, **kwargs):
        self.user = user
        Task = models.Task
        if 'instance' not in kwargs:
            kwargs['instance'] = Task(author=user, is_abs=False)
        super(TaskCreateForm, self).__init__(*args, **kwargs)
        # Set first tracker.
        tracker_field = self.fields['tracker']
        tracker_field.empty_label = None
        # Only NEW stages is accessible.
        Stage = models.Stage
        qs = Stage.objects.filter(status=Stage.STATUS_NEW)
        stage_field = self.fields['stage']
        stage_field.empty_label = None
        stage_field.queryset = qs
        # Only not closed task may be as parent.
        statuses = Stage.STATUSES_FOR_PARENTS
        qs = Task.objects.filter(stage__status__in=statuses)
        self.fields['parent'].queryset = qs
        # Only not released version may be is set.
        qs = models.Version.objects.filter(
            Q(release_date__isnull=True) | Q(release_date__gte=now()))
        self.fields['version'].queryset = qs
        # Only NEW attachements is accessible.
        Attachment = models.Attachment
        qs = Attachment.objects.get_free().filter(user=user)
        attachments_field = self.fields['attachments']
        attachments_field.queryset = qs

    def get_planned_time(self, data):
        days = data['planned_days'] or 0
        hours = data['planned_hours'] or 0
        minutes = data['planned_minutes'] or 0
        return (days * 86400 + hours * 3600 + minutes * 60) or None

    def clean(self):
        data = super().clean()
        data['planned_time'] = self.get_planned_time(data)
        self.similarity_checking(data)
        return data


class TaskChangeForm(CheckTaskMixing, forms.ModelForm):

    planned_start = IsoDateTimeField(required=False, label=_('Начало работ'))
    planned_time = forms.IntegerField(
        required=False, widget=forms.HiddenInput())
    planned_days = forms.IntegerField(
        required=False, label=_('Дней по плану'), min_value=1)
    planned_hours = forms.IntegerField(
        required=False, label=_('Часов по плану'), min_value=1, max_value=23)
    planned_minutes = forms.IntegerField(
        required=False, label=_('Минут по плану'), min_value=1, max_value=59)
    comment = forms.CharField(
        required=False, label=_('Комментарий'), widget=forms.Textarea())

    class Meta:
        model = models.Task
        exclude = [
            'created', 'updated', 'author', 'is_abs',
        ]
        widgets = {
            'parent': Selectize(refurl='proplan:api:tasks', attrs={
                'data-pk': 'id',
                'data-label': 'title',
                'data-fields': 'title',
            }),
            'leader': Selectize(),
            'subproject': Selectize(),
            'version': Selectize(),
            'attachments': SelectizeMultiple(),
        }

    def __init__(self, user, *args, **kwargs):
        self.user = user
        assert 'instance' in kwargs
        super(TaskChangeForm, self).__init__(*args, **kwargs)
        # Set days, hours and minutes.
        time = self.instance.planned_time or 0
        days = time // 86400
        time -= days * 86400
        hours = time // 3600
        time -= hours * 3600
        minutes = time // 60
        self.fields['planned_days'].initial = days or None
        self.fields['planned_hours'].initial = hours or None
        self.fields['planned_minutes'].initial = minutes or None

    def make_diff(self, data):
        self.diff = diff = {}
        instance = self.instance
        for name in self.changed_data:
            if name == 'comment':
                continue
            if name == 'attachments':
                old = list(instance.attachments.values_list('id', flat=True))
                new = data.get(name)
                if new is None:
                    new = []
                else:
                    new = list(new.values_list('id', flat=True))
                old.sort()
                new.sort()
            else:
                old = getattr(instance, name)
                new = data.get(name)
                if old is not None:
                    old = force_text(old)
                if new is not None:
                    new = force_text(new)
            diff[name] = [old, new]
        return diff

    def get_planned_time(self, data):
        days = data['planned_days'] or 0
        hours = data['planned_hours'] or 0
        minutes = data['planned_minutes'] or 0
        return (days * 86400 + hours * 3600 + minutes * 60) or None

    def clean(self):
        data = super().clean()
        data['planned_time'] = self.get_planned_time(data)
        self.similarity_checking(data)
        self.make_diff(data)
        return data

    def save(self, *args, **kwargs):
        task = super().save(*args, **kwargs)
        message = self.cleaned_data['comment'].strip()
        diff = self.diff
        if message or diff:
            changes = json.dumps(diff)
            comment = models.Comment(
                user=self.user, task=task,
                message=message, changes=changes)
            comment.save()
        return task


class ABSTaskCreateForm(CheckTaskMixing, forms.ModelForm):
    """
    Form for creating task from Automaic Bug System.

    """

    class Meta:
        model = models.Task
        fields = ['parent', 'title', 'description', 'attachments']
        widgets = {
            'parent': forms.NumberInput(),
        }

    def __init__(self, *args, **kwargs):
        if 'instance' not in kwargs:
            kwargs['instance'] = models.Task(
                is_abs=True,
                tracker=models.Tracker.objects.get_abs(),
                stage=models.Stage.objects.get_abs(),
            )
        super(ABSTaskCreateForm, self).__init__(*args, **kwargs)

    def clean_tracker(self):
        tracker = self.cleaned_data['tracker']
        if not tracker.is_abs:
            raise forms.ValidationError(_('Трекер не для ABS.'))
        return tracker

    def clean_stage(self):
        stage = self.cleaned_data['stage']
        if not stage.is_abs:
            raise forms.ValidationError(_('Стадия не для ABS.'))
        return stage

    def clean(self):
        instance = self.instance
        if not (instance.tracker and instance.stage):
            raise forms.ValidationError(
                _('ABS не готова принимать такие ошибки.'))
        return super().clean()


class CommentCreateForm(forms.ModelForm):

    class Meta:
        model = models.Comment
        fields = ['task', 'message', 'attachments']
        widgets = {
            'task': forms.HiddenInput(),
        }

    def __init__(self, user, *args, **kwargs):
        self.user = user
        if 'instance' not in kwargs:
            kwargs['instance'] = models.Comment(user=user)
        super(CommentCreateForm, self).__init__(*args, **kwargs)


class CommentChangeForm(forms.ModelForm):

    class Meta:
        model = models.Comment
        fields = ['message', 'attachments']

    def __init__(self, user, *args, **kwargs):
        self.user = user
        assert 'instance' in kwargs
        super(CommentChangeForm, self).__init__(*args, **kwargs)


class ExecutorCreateForm(forms.ModelForm):

    class Meta:
        model = models.Executor
        fields = ['task', 'user', 'role']
        widgets = {
            'task': forms.NumberInput(),
        }

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(ExecutorCreateForm, self).__init__(*args, **kwargs)


class ExecutorChangeForm(forms.ModelForm):

    class Meta:
        model = models.Executor
        fields = ['role']

    def __init__(self, user, *args, **kwargs):
        self.user = user
        assert 'instance' in kwargs
        super(ExecutorChangeForm, self).__init__(*args, **kwargs)
