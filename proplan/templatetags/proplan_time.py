from django import template
from django.utils.translation import ngettext

register = template.Library()


@register.filter
def timeprofit(seconds):
    if not seconds:
        return ''
    value = abs(seconds)
    if seconds > 0:
        sign = '+'
    elif seconds < 0:
        sign = '-'
    kw = {'sign': sign}
    if value < 60:
        kw['seconds'] = value
        return ngettext(
            '%(sign)s%(seconds)s секунда',
            '%(sign)s%(seconds)s секунд',
            kw['seconds']
        ) % kw
    elif value < 3600:
        kw['minutes'] = value // 60
        return ngettext(
            '%(sign)s%(minutes)s минута',
            '%(sign)s%(minutes)s минут',
            kw['minutes']
        ) % kw
    elif value < 86400:
        kw['hours'] = value // 3600
        return ngettext(
            '%(sign)s%(hours)s час',
            '%(sign)s%(hours)s часов',
            kw['hours']
        ) % kw
    elif value < 2592000:
        kw['days'] = value // 86400
        return ngettext(
            '%(sign)s%(days)s день',
            '%(sign)s%(days)s дней',
            kw['days']
        ) % kw
    kw['months'] = value // 2592000
    return ngettext(
        '%(sign)s%(months)s месяц',
        '%(sign)s%(months)s месяцев',
        kw['months']
    ) % kw
