#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
import os

from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase

from proplan import logic, models


BASE_DIR = os.path.dirname(os.path.abspath(__file__))


class AttachTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user('username')

    def delete_object(self, obj):
        path = obj.file.path
        self.assertTrue(os.path.exists(path))
        obj.delete()
        self.assertFalse(os.path.exists(path))

    def test_create_attachment_txt(self):
        file = SimpleUploadedFile('test.txt', b'testing file content')
        obj = logic.create_attachment(self.user, file)
        self.delete_object(obj)

    def test_create_attachment_png(self):
        filename = os.path.join(BASE_DIR, 'img', 'test.png')
        file = SimpleUploadedFile(
            'test.png',
            content=open(filename, 'rb').read(),
            content_type='image/png',
        )
        obj = logic.create_attachment(self.user, file)
        self.delete_object(obj)

    def test_create_attachment_jpg(self):
        filename = os.path.join(BASE_DIR, 'img', 'test.jpg')
        file = SimpleUploadedFile(
            'test.jpg',
            content=open(filename, 'rb').read(),
            content_type='image/jpg',
        )
        obj = logic.create_attachment(self.user, file)
        self.delete_object(obj)

    def test_create_attachment_jpeg(self):
        filename = os.path.join(BASE_DIR, 'img', 'test.jpeg')
        file = SimpleUploadedFile(
            'test.jpeg',
            content=open(filename, 'rb').read(),
            content_type='image/jpg',
        )
        obj = logic.create_attachment(self.user, file)
        self.delete_object(obj)

    def test_create_attachment_bmp(self):
        filename = os.path.join(BASE_DIR, 'img', 'test.bmp')
        file = SimpleUploadedFile(
            'test.bmp',
            content=open(filename, 'rb').read(),
            content_type='image/bmp',
        )
        obj = logic.create_attachment(self.user, file)
        self.delete_object(obj)

    def test_create_attachment_svg(self):
        filename = os.path.join(BASE_DIR, 'img', 'test.svg')
        file = SimpleUploadedFile(
            'test.svg',
            content=open(filename, 'rb').read(),
            content_type='image/svg+xml',
        )
        obj = logic.create_attachment(self.user, file)
        self.delete_object(obj)


class SimilarityTestCase(TestCase):

    def test_checking(self):
        func = logic.similarity_checking
        L = func('Test bug', '', is_abs=True, tracker=None)
        self.assertEqual(L, [])
        # TODO: continue realization


class ControllerTestCase(TestCase):

    def test_init(self):
        ctrl = logic.Controller(models.Task)
        self.assertIsNone(ctrl.search_fields)
        self.assertIsNone(ctrl.ordering_fields)
        self.assertIsNone(ctrl.filtering_fields)
        self.assertEqual(ctrl.exclude_fields, ('password',))
        self.assertIsNotNone(ctrl.fields)
