#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
import os
import unittest

from django.contrib.auth.models import User, Permission
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase

from proplan import models

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


class ParameterTestCase(TestCase):
    model = models.Parameter

    def test_create_str(self):
        """Check the installation of a string parmeter."""
        param = self.model(
            name='test_value',
            data='  \n   test     data   \n     \t   \n',
            datatype=self.model.VALUE_STRING,
        )
        param.save()
        self.assertEqual(param.data, 'test data')

    def test_create_int(self):
        """Check the installation of a integer parmeter."""
        param = self.model(
            name='test_value',
            data='1',
            datatype=self.model.VALUE_INTEGER,
        )
        param.save()
        self.assertEqual(param.value, 1)

    def test_create_float(self):
        """Check the installation of a float parmeter."""
        param = self.model(
            name='test_value',
            data='99.999',
            datatype=self.model.VALUE_FLOAT,
        )
        param.save()
        self.assertEqual(param.value, 99.999)

    def test_prepare_data(self):
        """Verifying the installation of pre-treatment parameters."""
        param = self.model(
            name='test_value',
            data='  \n   test     data   \n     \t   \n',
            datatype=self.model.VALUE_STRING,
        )
        param.prepare_data()
        self.assertEqual(param.data, 'test data')

        param.data = '  \n   ',
        with self.assertRaises(AssertionError):
            param.prepare_data()

    def test_get_value_for_system(self):
        """Check the method for obtaining system parameter by key."""
        param = self.model(
            name='test_value',
            data='string',
            datatype=self.model.VALUE_STRING,
        )
        param.save()
        self.assertEqual(self.model.get_value('test_value'), 'string')

    def test_get_value_for_user(self):
        """Check the method for obtaining user parameter by key."""
        user = User.objects.create_user('username')
        param = self.model(
            name='test_value',
            user=user,
            data='string',
            datatype=self.model.VALUE_STRING,
        )
        param.save()
        self.assertEqual(self.model.get_value('test_value', user.id), 'string')

    def test_get_all_params(self):
        """
        Check the method for obtaining all parameters in dictionary.
        """
        user = User.objects.create_user('username')
        # No parameters for system
        data = self.model.get_all_params()
        self.assertEqual(data, {})
        # No parameters for user
        data = self.model.get_all_params(user.id)
        self.assertEqual(data, {})
        # Add 1 system parameter.
        param = self.model(
            name='test_system_value',
            data='string',
            datatype=self.model.VALUE_STRING,
        )
        param.save()
        # Add 1 user parameter.
        param = self.model(
            name='test_user_value',
            user=user,
            data='string',
            datatype=self.model.VALUE_STRING,
        )
        param.save()
        # Checking.
        self.assertEqual(self.model.get_all_params(), {
            'test_system_value': 'string',
        })
        self.assertEqual(self.model.get_all_params(user.id), {
            'test_user_value': 'string',
        })


class AttachmentTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user('username')
        self.txt = SimpleUploadedFile('test.txt', b'testing file content')
        filename = os.path.join(BASE_DIR, 'img', 'test.jpg')
        self.jpg = SimpleUploadedFile(
            'test.jpg',
            content=open(filename, 'rb').read(),
            content_type='image/jpg',
        )

    def delete_object(self, obj):
        path = obj.file.path
        self.assertTrue(os.path.exists(path))
        obj.delete()
        self.assertFalse(os.path.exists(path))

    def test_create(self):
        obj = models.Attachment.objects.create(file=self.txt)
        obj.full_clean()
        self.assertIsNotNone(obj.created)
        self.assertIsNotNone(obj.updated)
        self.assertIsNone(obj.user)
        self.assertIsNotNone(obj.file.path)
        self.assertIsNotNone(obj.file.url)
        # default properties
        self.assertTrue(obj.is_abs)
        self.assertFalse(obj.is_image)
        self.assertFalse(obj.use_thumbnail)
        # cleaning
        self.delete_object(obj)

    def test_manager_get_free(self):
        manager = models.Attachment.objects
        attachment_abs = manager.create(file=self.txt)
        attachment1 = manager.create(user=self.user, file=self.txt)
        attachment2 = manager.create(user=self.user, file=self.txt)
        attachment3 = manager.create(user=self.user, file=self.txt)
        attachment4 = manager.create(user=self.user, file=self.txt)
        # 5 all, 4 users
        self.assertEqual(manager.get_free().count(), 5)
        self.assertEqual(manager.get_free().filter(user=self.user).count(), 4)
        # add to tracker
        tracker = models.Tracker.objects.create(name='bug')
        stage = models.Stage.objects.create(name='new')
        task = models.Task.objects.create(
            author=self.user,
            tracker=tracker,
            stage=stage,
            title='Title',
        )
        task.attachments.add(attachment1, attachment2)
        # 3 all, 2 users
        self.assertEqual(manager.get_free().count(), 3)
        self.assertEqual(manager.get_free().filter(user=self.user).count(), 2)
        # cleaning
        self.delete_object(attachment_abs)
        self.delete_object(attachment1)
        self.delete_object(attachment2)
        self.delete_object(attachment3)
        self.delete_object(attachment4)

    def test_get_thumb_filename(self):
        # for model
        method = models.Attachment.get_thumb_filename
        self.assertEqual(method(''), '.thumb')
        self.assertEqual(method('test'), 'test.thumb')
        self.assertEqual(method('test.jpg'), 'test.thumb.jpg')
        # for object
        obj = models.Attachment.objects.create(file=self.txt, user=self.user)
        method = obj.get_thumb_filename
        self.assertEqual(method(''), '.thumb')
        self.assertEqual(method('test'), 'test.thumb')
        self.assertEqual(method('test.jpg'), 'test.thumb.jpg')
        # cleaning
        self.delete_object(obj)

    def test_txt_properties(self):
        obj = models.Attachment.objects.create(file=self.txt, user=self.user)
        self.assertFalse(obj.is_abs)
        self.assertFalse(obj.is_image)
        self.assertFalse(obj.use_thumbnail)
        self.assertEqual(obj.name, 'test.txt')
        self.assertEqual(obj.url, obj.file.url)
        self.assertEqual(obj.thumb_url, '')
        self.assertEqual(obj.path, obj.file.path)
        self.assertEqual(obj.thumb_path, '')
        # cleaning
        self.delete_object(obj)

    def test_jpg_properties(self):
        obj = models.Attachment.objects.create(file=self.jpg, user=self.user)
        method = obj.get_thumb_filename
        self.assertFalse(obj.is_abs)
        self.assertTrue(obj.is_image)
        self.assertTrue(obj.use_thumbnail)
        self.assertEqual(obj.name, 'test.jpg')
        self.assertEqual(obj.url, obj.file.url)
        self.assertEqual(obj.thumb_url, method(obj.file.url))
        self.assertEqual(obj.path, obj.file.path)
        self.assertEqual(obj.thumb_path, method(obj.file.path))
        # cleaning
        self.delete_object(obj)


class TrackerTestCase(TestCase):

    def test_create(self):
        obj = models.Tracker.objects.create(name='Name', is_abs=True)
        obj.full_clean()
        self.assertEqual(obj.name, 'Name')
        self.assertEqual(obj.description, '')
        self.assertEqual(obj.status, 1)
        self.assertTrue(obj.is_abs)

    def test_statuses(self):
        obj = models.Tracker()

        self.assertEqual(obj.status, obj.STATUS_WISH)
        self.assertTrue(obj.is_wish)
        self.assertFalse(obj.is_need)
        self.assertFalse(obj.is_defect)

        obj.status = obj.STATUS_NEED
        self.assertFalse(obj.is_wish)
        self.assertTrue(obj.is_need)
        self.assertFalse(obj.is_defect)

        obj.status = obj.STATUS_DEFECT
        self.assertFalse(obj.is_wish)
        self.assertFalse(obj.is_need)
        self.assertTrue(obj.is_defect)


class StageTestCase(TestCase):

    def test_create(self):
        obj = models.Stage.objects.create(name='Name')
        obj.full_clean()
        self.assertEqual(obj.name, 'Name')
        self.assertEqual(obj.description, '')
        self.assertEqual(obj.status, 1)
        self.assertFalse(obj.is_abs)

    def test_statuses(self):
        obj = models.Stage()

        self.assertEqual(obj.status, obj.STATUS_NEW)
        self.assertTrue(obj.is_new)
        self.assertFalse(obj.is_error)
        self.assertFalse(obj.is_process)
        self.assertFalse(obj.is_pause)
        self.assertFalse(obj.is_finish)

        obj.status = obj.STATUS_ERROR
        self.assertFalse(obj.is_new)
        self.assertTrue(obj.is_error)
        self.assertFalse(obj.is_process)
        self.assertFalse(obj.is_pause)
        self.assertFalse(obj.is_finish)

        obj.status = obj.STATUS_PROCESS
        self.assertFalse(obj.is_new)
        self.assertFalse(obj.is_error)
        self.assertTrue(obj.is_process)
        self.assertFalse(obj.is_pause)
        self.assertFalse(obj.is_finish)

        obj.status = obj.STATUS_PAUSE
        self.assertFalse(obj.is_new)
        self.assertFalse(obj.is_error)
        self.assertFalse(obj.is_process)
        self.assertTrue(obj.is_pause)
        self.assertFalse(obj.is_finish)

        obj.status = obj.STATUS_FINISH
        self.assertFalse(obj.is_new)
        self.assertFalse(obj.is_error)
        self.assertFalse(obj.is_process)
        self.assertFalse(obj.is_pause)
        self.assertTrue(obj.is_finish)


class SubprojectTestCase(TestCase):

    def test_create(self):
        obj = models.Subproject.objects.create(name='Name')
        obj.full_clean()
        self.assertEqual(obj.name, 'Name')
        self.assertEqual(obj.description, '')


class RoleTestCase(TestCase):

    def test_create(self):
        obj = models.Role.objects.create(name='Name')
        obj.full_clean()
        self.assertEqual(obj.name, 'Name')
        self.assertEqual(obj.description, '')
        obj.users.add(User.objects.create_user('username'))
        obj.permissions.add(*Permission.objects.all()[:2])


class VersionTestCase(TestCase):

    def test_create(self):
        obj = models.Version.objects.create()
        obj.full_clean()
        self.assertEqual(obj.major, 0)
        self.assertEqual(obj.minor, 0)
        self.assertEqual(obj.bugfix, '')
        self.assertIsNone(obj.planned_date)
        self.assertIsNone(obj.release_date)
        self.assertIsNone(obj.master)
        self.assertEqual(obj.description, '')

    def test_version(self):
        obj = models.Version()
        self.assertEqual(obj.version, '0.0')
        obj.major = 1
        self.assertEqual(obj.version, '1.0')
        obj.minor = 1
        self.assertEqual(obj.version, '1.1')
        obj.bugfix = 'alpha1'
        self.assertEqual(obj.version, '1.1.alpha1')

    @unittest.skip('NotImplemented')
    def test_make_changelog(self):
        pass

    @unittest.skip('NotImplemented')
    def test_count_total_tasks(self):
        pass

    @unittest.skip('NotImplemented')
    def test_count_open_tasks(self):
        pass

    @unittest.skip('NotImplemented')
    def test_count_reject_tasks(self):
        pass

    @unittest.skip('NotImplemented')
    def test_count_finish_tasks(self):
        pass

    @unittest.skip('NotImplemented')
    def test_readiness(self):
        pass

    def test_get_master_full_name(self):
        obj = models.Version()
        self.assertEqual(obj.get_master_full_name(), 'отсутствует')
        master = User.objects.create_user('master')
        obj.master = master
        self.assertEqual(obj.get_master_full_name(), 'master')
        master.first_name = 'John'
        master.last_name = 'Bon Jovy'
        self.assertEqual(obj.get_master_full_name(), 'John Bon Jovy')


class TaskTestCase(TestCase):

    def test_create(self):
        author = User.objects.create_user('author')
        parent = None
        tracker = models.Tracker.objects.create(name='bug')
        stage = models.Stage.objects.create(name='new')
        obj = models.Task.objects.create(
            author=author,
            parent=parent,
            tracker=tracker,
            stage=stage,
            title='Title',
        )
        self.assertIsNotNone(obj.created)
        self.assertIsNotNone(obj.updated)
        self.assertEqual(obj.author, author)
        self.assertIsNone(obj.leader)
        self.assertEqual(obj.parent, parent)
        self.assertIsNone(obj.subproject)
        self.assertIsNone(obj.version)
        self.assertEqual(obj.tracker, tracker)
        self.assertEqual(obj.stage, stage)
        self.assertEqual(obj.priority, 1)
        self.assertFalse(obj.is_abs)
        self.assertEqual(obj.title, 'Title')
        self.assertEqual(obj.description, '')
        self.assertIsNone(obj.planned_start)
        self.assertIsNone(obj.planned_time)
        self.assertEqual(obj.attachments.count(), 0)
        # default properties
        self.assertEqual(obj.time_total, 0)
        self.assertEqual(obj.time_work, 0)

    @unittest.skip('NotImplemented')
    def test_time_total(self):
        pass

    @unittest.skip('NotImplemented')
    def test_time_work(self):
        pass

    @unittest.skip('NotImplemented')
    def test_start(self):
        pass

    @unittest.skip('NotImplemented')
    def test_stop(self):
        pass

    @unittest.skip('NotImplemented')
    def test_deadline(self):
        pass

    @unittest.skip('NotImplemented')
    def test_get_executors_names(self):
        pass

    def test_get_author_full_name(self):
        author = User.objects.create_user('author')
        obj = models.Task(author=author)
        self.assertEqual(obj.get_author_full_name(), 'author')
        author.first_name = 'John'
        author.last_name = 'Bon Jovy'
        self.assertEqual(obj.get_author_full_name(), 'John Bon Jovy')

    def test_leader_full_name(self):
        obj = models.Task()
        self.assertEqual(obj.get_leader_full_name(), 'отсутствует')
        leader = User.objects.create_user('leader')
        obj.leader = leader
        self.assertEqual(obj.get_leader_full_name(), 'leader')
        leader.first_name = 'John'
        leader.last_name = 'Bon Jovy'
        self.assertEqual(obj.get_leader_full_name(), 'John Bon Jovy')


class CommentTestCase(TestCase):

    def test_create(self):
        author = User.objects.create_user('author')
        tracker = models.Tracker.objects.create(name='bug')
        stage = models.Stage.objects.create(name='new')
        task = models.Task.objects.create(
            author=author,
            tracker=tracker,
            stage=stage,
            title='Title',
        )
        obj = models.Comment.objects.create(
            task=task,
            user=author,
            message='Comment',
        )
        self.assertIsNotNone(obj.created)
        self.assertIsNotNone(obj.updated)
        self.assertEqual(obj.task, task)
        self.assertEqual(obj.user, author)
        self.assertEqual(obj.message, 'Comment')
        self.assertEqual(obj.attachments.count(), 0)


class ExecutorTestCase(TestCase):

    def test_create(self):
        author = User.objects.create_user('author')
        tracker = models.Tracker.objects.create(name='bug')
        stage = models.Stage.objects.create(name='new')
        task = models.Task.objects.create(
            author=author,
            tracker=tracker,
            stage=stage,
            title='Title',
        )
        obj = models.Executor.objects.create(
            task=task,
            user=author,
            role=None,
        )
        self.assertIsNotNone(obj.created)
        self.assertIsNotNone(obj.updated)
        self.assertEqual(obj.task, task)
        self.assertEqual(obj.user, author)
        self.assertEqual(obj.role, None)
        self.assertIsNone(obj.start)
        self.assertIsNone(obj.stop)
        self.assertIsNone(obj.sprint)
        self.assertEqual(obj.time_sprints, 0)
        # default properties
        self.assertEqual(obj.time_total, 0)
        self.assertEqual(obj.time_work, 0)
        self.assertFalse(obj.is_current)

    @unittest.skip('NotImplemented')
    def test_time_total(self):
        pass

    @unittest.skip('NotImplemented')
    def test_time_work(self):
        pass
