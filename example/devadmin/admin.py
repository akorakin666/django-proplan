from django.contrib import admin
from proplan import models

site = admin.site

site.register(models.Parameter)
site.register(models.Attachment)
site.register(models.Tracker)
site.register(models.Stage)
site.register(models.Role)
site.register(models.Version)
site.register(models.Task)
site.register(models.Comment)
site.register(models.Executor)
